#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Trojx on 2019/1/7
from flask import Flask, request, jsonify
from PIL import Image as pil_image
import numpy as np
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.preprocessing import image
import tensorflow as tf

app = Flask(__name__)

MODEL_FILE_PATH = 'weights_2018-12-21 15:47:15.h5'
labels = ['anoplophora_chinesis', 'bradybaena_similaris', 'chondracris_rosea', 'cryptotympana_atrata',
          'papilio_polytes']
chinese_names = ['星天牛', '同型巴蜗牛', '棉蝗', '蟪蛄', '玉带凤蝶']

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=(150, 150, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(5))
model.add(Activation('softmax'))

model.load_weights(MODEL_FILE_PATH)
graph = tf.get_default_graph()  # https://github.com/fchollet/keras/issues/2397#issuecomment-254919212


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/image_classifier', methods=['POST'])
def image_classifier():
    f = request.files['image']
    img = pil_image.open(f)
    img = img.resize((150, 150))
    x = image.img_to_array(img)
    x = x / 255
    x = np.expand_dims(x, axis=0)
    global graph
    with graph.as_default():
        y = model.predict(x)
    print('分类:', chinese_names[np.argmax(y[0])], '(', labels[np.argmax(y[0])], ')')
    print('各类别可能性：')
    possibilities = []
    for label, name, weight in zip(labels, chinese_names, y[0]):
        possibilities.append({
            'label': label,
            'name': name,
            'weight': weight.tolist()
        })
        print(name, '\t', int(weight * 100), '%')
    return jsonify({
        'result': {
            'label': labels[np.argmax(y[0])],
            'name': chinese_names[np.argmax(y[0])]
        },
        'possibilities': possibilities
    })


if __name__ == '__main__':
    app.run(host='0.0.0.0')
