#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-12-17 下午5:31
# @Author  : Trojx
# @File    : image_classifier.py
# https://keras-cn-docs.readthedocs.io/zh_CN/latest/blog/image_classification_using_very_little_data/
# https://blog.csdn.net/JinbaoSite/article/details/77435558

from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense

from keras.preprocessing.image import ImageDataGenerator
from keras.utils import plot_model
from keras.callbacks import TensorBoard

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=(150, 150, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(5))  # 3分类
model.add(Activation('softmax'))  # 多分类用此激活函数（https://www.zhihu.com/question/40403377）

model.compile(loss='categorical_crossentropy',  # 多分类
              optimizer='rmsprop',
              metrics=['accuracy'])
plot_model(model, 'model.png', show_shapes=True)  # 输出模型图片
# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

# this is a generator that will read pictures found in
# subfolers of 'data/train', and indefinitely generate
# batches of augmented image data
train_generator = train_datagen.flow_from_directory(
    'data/train',  # this is the target directory
    target_size=(150, 150),  # all images will be resized to 150x150
    batch_size=32,
    class_mode='categorical')  # 多分类

# this is a similar generator, for validation data
validation_generator = test_datagen.flow_from_directory(
    'data/validation',
    target_size=(150, 150),
    batch_size=32,
    class_mode='categorical')  # 多分类

tb_callback = TensorBoard(log_dir='./logs',  # log 目录
                          histogram_freq=1,  # 按照何等频率（epoch）来计算直方图，0为不计算
                          batch_size=32,  # 用多大量的数据计算直方图
                          write_graph=True,  # 是否存储网络结构图
                          write_grads=True,  # 是否可视化梯度直方图
                          write_images=True,  # 是否可视化参数
                          embeddings_freq=0,
                          embeddings_layer_names=None,
                          embeddings_metadata=None)

model.fit_generator(
    train_generator,
    samples_per_epoch=5000,
    nb_epoch=50,
    validation_data=validation_generator,
    nb_val_samples=500,
    callbacks=[tb_callback])
model.save_weights('weights.h5')  # always save your weights after training or during training
