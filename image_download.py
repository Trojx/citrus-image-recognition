#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Trojx on 2018/12/14
from google_images_download import google_images_download

response = google_images_download.googleimagesdownload()
absolute_image_paths = response.download({
    'keywords': '玉带凤蝶',
    'chromedriver': 'bin/chromedriver',
    'proxy': '127.0.0.1:1087',
    'limit': 120
})
print(absolute_image_paths)
