#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-12-28 下午6:53
# @Author  : Trojx
# @File    : python_hdfs.py
from hdfs import *
from datetime import datetime

# 这里用Client会存在用户权限问题
client = InsecureClient('http://127.0.0.1:50070')

# 在本地创建文本文件myfile
with open('myfile.txt', 'w') as f:
    f.write('china cstor cstor cstor china')

# 将文件上传至HDFS
client.upload('/trojx', 'myfile.txt', overwrite=True)

# 读取HDFS里文件的内容
with client.read('/trojx/myfile.txt') as reader:
    print('文件内容:\n\t', reader.read())

# 获取并输出HDFS里文件相关属性
status = client.status('/trojx/myfile.txt')
print('文件信息：\n\t文件大小：{0},拥有者:{1},集群副本数:{2},最近修改时间:{3}'
      .format(status['length'], status['owner'], status['replication'],
              datetime.fromtimestamp(status['modificationTime'] / 1000)))