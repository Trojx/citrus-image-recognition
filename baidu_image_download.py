#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-12-17 下午1:24
# @Author  : Trojx
# @File    : baidu_image_download.py
# coding=utf-8
import requests
import re
import os

# 搜索请求页的请求头
headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7',
    'Connection': 'keep-alive',
    'DNT': '1',
    'Host': 'image.baidu.com',
    'Referer': 'http://image.baidu.com',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.80 '
                  'Chrome/71.0.3578.80 Safari/537.36',
}
# 在线图片的请求头
bd_thumb_headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7',
    'Cache-Control': 'max-age=0',
    'Connection': 'keep-alive',
    'DNT': '1',
    'Host': 'img1.imgtn.bdimg.com',
    'If-Modified-Since': 'Thu, 01 Jan 1970 00:00:00 GMT',
    'Referer': 'http://image.baidu.com/search/flip?tn=baiduimage&word=%E7%8E%89%E5%B8%A6%E5%87%A4%E8%9D%B6&pn=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu '
                  'Chromium/71.0.3578.80 Chrome/71.0.3578.80 Safari/537.36',
}


def get_img_urls(keyword, limit=100, type='origin'):
    """
    获取当前关键词的所有图片链接
    :param keyword: 关键词
    :param limit: 图片数量
    :param type: 类型 （origin:原图,thumbnail:缩略图）
    :return:
    """
    img_urls = []
    for pn in range(0, limit, 60):
        resp = requests.get('http://image.baidu.com/search/flip', params={
            'tn': 'baiduimage',
            'word': keyword,
            'pn': pn,
        })
        thumb_urls = re.findall('"thumbURL":"(.*?)",', resp.text, re.S)
        origin_urls = re.findall('"objURL":"(.*?)",', resp.text, re.S)
        if type == 'origin':
            img_urls.extend(origin_urls)
        elif type == 'thumbnail':
            img_urls.extend(thumb_urls)
    return img_urls[:limit]


def save_imgs(img_urls, key, img_dir='./downloads'):
    """
    将在线图片保存至本地
    :param img_urls: 图片链接列表
    :param key: 关键词
    :param img_dir: 图片文件根目录
    :return:
    """
    img_dir = os.path.join(img_dir, key)
    if not os.path.exists(img_dir):
        os.makedirs(img_dir)
    for i, img_url in enumerate(img_urls):
        try:
            print('Downloading img,url={0}'.format(img_url))
            save_img(img_url, os.path.join(img_dir, str(i) + '.jpg'))
        except Exception as e:
            print('Downloading img error,url={0},error={1}'.format(img_url, e))


def save_img(url, img_path):
    """
    将一张在线图片下载至本地保存
    :param url: 图片链接
    :param img_path: 本地文件路径
    :return:
    """
    if 'bdimg.com' in url:
        img_headers = bd_thumb_headers
    else:
        img_headers = {}
    r = requests.get(url, stream=True, headers=img_headers)
    with open(img_path, 'wb') as fd:
        for chunk in r.iter_content():
            fd.write(chunk)


if __name__ == '__main__':
    imgs = get_img_urls('蜗牛', limit=1000, type='thumbnail')
    save_imgs(imgs, key='蜗牛')
