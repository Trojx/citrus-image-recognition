#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-12-19 下午6:28
# @Author  : Trojx
# @File    : image_file_op.py

import os
import shutil

if __name__ == '__main__':
    species_name = 'papilio_polytes'
    train_dir = os.path.join(os.getcwd(), 'data', 'train', species_name)
    validation_dir = os.path.join(os.getcwd(), 'data', 'validation', species_name)
    if not os.path.exists(train_dir):
        os.makedirs(train_dir)
    if not os.path.exists(validation_dir):
        os.makedirs(validation_dir)
    files = os.listdir('downloads/' + species_name)
    # files.sort()
    for i in range(0, 1000):
        src = os.path.join('downloads', species_name, files[i])
        file_ext = os.path.splitext(src)[-1]
        shutil.copy(src,
                    os.path.join('data', 'train', species_name, species_name + str(i).zfill(4) + file_ext))

    for i in range(1000, 1100):
        src = os.path.join('downloads', species_name, files[i])
        file_ext = os.path.splitext(src)[-1]
        shutil.copy(src,
                    os.path.join('data', 'validation', species_name, species_name + str(i - 1000).zfill(4) + file_ext))
