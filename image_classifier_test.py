#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-12-20 下午12:00
# @Author  : Trojx
# @File    : image_classifier_test.py
import numpy as np
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.preprocessing import image

labels = ['anoplophora_chinesis', 'bradybaena_similaris', 'chondracris_rosea', 'cryptotympana_atrata',
          'papilio_polytes']
chinese_names = ['星天牛', '同型巴蜗牛', '棉蝗', '蟪蛄', '玉带凤蝶']

file_path = 'downloads/papilio_polytes/4. butterfly-yu-d01-2.jpg'  # 待预测的图片路径

img = image.load_img(file_path, target_size=(150, 150))
x = image.img_to_array(img)
x = x / 255  # 否则输出[1. 0. 0.] https://stackoverflow.com/questions/45900449/keras-model-predict-always-0/46331070
x = np.expand_dims(x, axis=0)

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=(150, 150, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(5))  # 5分类
model.add(Activation('softmax'))  # 多分类用此激活函数（https://www.zhihu.com/question/40403377）

model.load_weights('weights_2018-12-21 15:47:15.h5')
y = model.predict(x)
print('分类:', chinese_names[np.argmax(y[0])], '(', labels[np.argmax(y[0])], ')')
print('各类别可能性：')
for name, weight in zip(chinese_names, y[0]):
    print(name, '\t', int(weight * 100), '%')
