#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-12-27 下午2:36
# @Author  : Trojx
# @File    : image_preprocess.py
# 数据预处理和数据提升

import os
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

datagen = ImageDataGenerator(
    rotation_range=40,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest')

img = load_img('data/validation/papilio_polytes/papilio_polytes0072.jpeg')
x = img_to_array(img)
x = x.reshape((1,) + x.shape)

preview_path = os.path.join(os.getcwd(), 'preview')
if not os.path.exists(preview_path):
    os.mkdir(preview_path)

i = 0
for batch in datagen.flow(x, batch_size=1,
                          save_to_dir='preview', save_prefix='papilio_polytes', save_format='jpeg'):
    i += 1
    if i > 20:
        break
