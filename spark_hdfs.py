#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-12-27 下午3:18
# @Author  : Trojx
# @File    : spark_hdfs.py
import os
import findspark

findspark.init(spark_home='/usr/local/spark')
from pyspark import SparkConf
from pyspark.context import SparkContext

sc = SparkContext.getOrCreate(SparkConf().setMaster("local[*]"))


def save_local_files_to_hdfs():
    """
    写入本地文件至HDFS
    :return:
    """
    for label in os.listdir('data/validation'):
        bfiles = sc.binaryFiles('file://' + os.path.join(os.getcwd(), 'data/validation', label))
        bfiles.saveAsHadoopFile('/trojx/' + 'data/validation/' + label,
                                'org.apache.hadoop.mapred.SequenceFileOutputFormat')
        print(bfiles)


def func_map(x):
    """
    map函数，获取rdd中的每一个二进制文件，并返回文件大小
    :param x:
    :return:
    """
    # print('file name:{0},file size:{1}'.format(x[0], len(x[1])))
    return len(x[1])


def func_reduce(x, y):
    """
    reduce函数，将同类别下的文件大小累加
    :param x:
    :param y:
    :return:
    """
    return x + y


def read_files_to_rdd(root_dir):
    """
    读取文件为RDD,并统计文件信息
    :return:
    """
    stats = []
    for label in os.listdir(root_dir):
        rdd = sc.binaryFiles('file://' + os.path.join(os.getcwd(), root_dir, label))
        total_size = rdd.map(func_map).reduce(func_reduce)
        stats.append((label, total_size))
    return stats


if __name__ == '__main__':
    train_stats = read_files_to_rdd('data/train')
    print('训练数据集统计：', sorted(train_stats))
    validation_stats = read_files_to_rdd('data/validation')
    print('验证数据集统计', sorted(validation_stats))
